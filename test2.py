from bs4 import BeautifulSoup
import requests
from collections import defaultdict

url = 'https://inlnk.ru/jElywR'

dict_list_words = defaultdict(list)
flag = True
mapping = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЭЮЯ'
while flag:
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')

    for item in soup.find(id='mw-pages').find_all('li'):
        if item.text[0] == 'A':
            flag = False
        if not flag:
            break
        if item.text[0] in mapping:
            dict_list_words[item.text[0]].append(item.text)

    for item in soup.find_all('a'):
        if item.text == 'Следующая страница':
            link = item.get('href')
            break

    url = "https://ru.wikipedia.org" + link

alphabet_counter_dict = {k: len(dict_list_words[k]) for k in mapping}
print(alphabet_counter_dict)
