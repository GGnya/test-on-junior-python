def task(array):
    number_of_first_zero = 0

    def recursive_find_zero(array):
        nonlocal number_of_first_zero
        mid_array = int(len(array) / 2)
        if array[mid_array] == '1':
            number_of_first_zero += mid_array
            return recursive_find_zero(array[mid_array:])
        else:
            if array[mid_array - 1] == '1':
                number_of_first_zero += mid_array
                return number_of_first_zero
            else:
                return recursive_find_zero(array[:mid_array])

    return recursive_find_zero(array)


array = '11111111111110000000000000000'

if __name__ == '__main__':
    print(task(array))
    print(array.find('0'))