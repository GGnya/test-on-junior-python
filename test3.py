def appearance(task):
    def interval_cleaning(intervals):
        final_interval_list = [0, 0]
        for index, item in enumerate(intervals):
            if index % 2 == 0:
                if item < final_interval_list[-1]:
                    if final_interval_list[-1] < intervals[index + 1]:
                        final_interval_list[-1] = intervals[index + 1]
                else:
                    final_interval_list.append(item)
            elif index % 2 != 0:
                if len(final_interval_list) % 2 == 1:
                    final_interval_list.append(item)

        return final_interval_list

    def is_intervals_intersect(first_interval, second_interval):
        if first_interval[0] <= second_interval[1] and first_interval[1] >= second_interval[0]:
            return True
        return False

    lesson_list = task['lesson']
    pupil_list = interval_cleaning(task['pupil'])
    tutor_list = interval_cleaning(task['tutor'])

    def func(first_interval_list, second_list):

        index_first_list_start = 0
        index_first_list_finish = 1
        index_second_list_start = 0
        index_second_list_finish = 1
        last_intervals = []
        try:
            while True:

                first_list_interval_tuple = (
                    first_interval_list[index_first_list_start], first_interval_list[index_first_list_finish])
                second_list_interval_tuple = (
                    second_list[index_second_list_start], second_list[index_second_list_finish])

                if is_intervals_intersect(first_list_interval_tuple, second_list_interval_tuple):
                    start_less = max(first_list_interval_tuple[0], second_list_interval_tuple[0])
                    final_less = min(first_list_interval_tuple[1], second_list_interval_tuple[1])
                    last_intervals.append((start_less, final_less))
                    if final_less == first_list_interval_tuple[1]:
                        index_first_list_start += 2
                        index_first_list_finish += 2
                    else:
                        index_second_list_start += 2
                        index_second_list_finish += 2

                else:
                    if first_list_interval_tuple[1] < second_list_interval_tuple[1]:
                        index_first_list_start += 2
                        index_first_list_finish += 2
                    else:
                        index_second_list_start += 2
                        index_second_list_finish += 2

        except IndexError:
            final_interval_list = []
            for item in last_intervals:
                for i in item:
                    final_interval_list.append(i)

            return final_interval_list

    lesson_tutor_list = func(lesson_list, tutor_list)
    lesson_tutor_pupil_list = func(lesson_tutor_list, pupil_list)
    study_time = sum([i for index, i in enumerate(lesson_tutor_pupil_list) if index % 2 == 1]) - sum(
        [i for index, i in enumerate(lesson_tutor_pupil_list) if index % 2 == 0])

    return study_time


tests = [
    {'data': {'lesson': [1594663200, 1594666800],
              'pupil': [1594663340, 1594663389, 1594663390, 1594663395, 1594663396, 1594666472],
              'tutor': [1594663290, 1594663430, 1594663443, 1594666473]},
     'answer': 3117
     },
    {'data': {'lesson': [1594702800, 1594706400],
              'pupil': [1594702789, 1594704500, 1594702807, 1594704542, 1594704512, 1594704513, 1594704564, 1594705150,
                        1594704581, 1594704582, 1594704734, 1594705009, 1594705095, 1594705096, 1594705106, 1594706480,
                        1594705158, 1594705773, 1594705849, 1594706480, 1594706500, 1594706875, 1594706502, 1594706503,
                        1594706524, 1594706524, 1594706579, 1594706641],
              'tutor': [1594700035, 1594700364, 1594702749, 1594705148, 1594705149, 1594706463]},
     'answer': 3577
     },
    {'data': {'lesson': [1594692000, 1594695600],
              'pupil': [1594692033, 1594696347],
              'tutor': [1594692017, 1594692066, 1594692068, 1594696341]},
     'answer': 3565
     },
]

if __name__ == '__main__':
    for i, test in enumerate(tests):
        test_answer = appearance(test['data'])
        assert test_answer == test['answer'], f'Error on test case {i}, got {test_answer}, expected {test["answer"]}'
